# python3.6 base
FROM python:3.6

RUN mkdir app

# copy all files into docker
COPY . /app/

# cd into the right file
WORKDIR /app

# install pipenv
RUN pip install pipenv

# install w/ pipenv
RUN pipenv install --deploy

# cd into the right file
WORKDIR /app/madsapp

# pipenv run python manage.py runserver
CMD [ "pipenv","run","uvicorn","--host", "0.0.0.0", "--port", "$PORT", "madsapp.asgi:application"]

