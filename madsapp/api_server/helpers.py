from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response

def catch_no_object(func):
    def wrapped_func(*args, **kwargs):
        # Before Function
        try:
            func_result = func(*args, **kwargs)
        except ObjectDoesNotExist as err:
            return Response(data={"error":"Requested object does not exist"},status=status.HTTP_404_NOT_FOUND)
        # After Function
        return func_result
    return wrapped_func

def get_token_from_headers(request)->str:
    headers = dict(request.headers)
    jwt:str = headers["Authorization"].split(" ")[1]
    return jwt
