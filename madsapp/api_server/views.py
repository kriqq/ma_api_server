import logging

from django.shortcuts import render
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from .models import User, Profile, Artwork, Order
from .serializers import *

from .authentication.firebase.firebase_auth import refresh_token, sign_up_and_login, login, logout, change_password, update_firebase_user_email, send_password_reset_email, delete_user, decode_unverified_token
from .authentication.firebase.models import *
from .permissions import *

from .helpers import get_token_from_headers

logger = logging.getLogger('cloudLogger')

#######################################
###########MODEL ENDPOINTS#############
#######################################

class UserView(APIView):

    @require_authentication
    def get(self, request):
        requesting_user = request.user

        try:
            check_can_read_all_users(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            users = User.objects.all()
            serialize = UsersReadSerializer(users, many=True)
            return Response(serialize.data)

    # UNPROTECTED
    def post(self, request):
        # User likely does not exist prior to creating user, unless superuser creating another user
        requesting_user = request.user

        data = request.data

        data['username'] = data['email']
        zipcode:str = data['zipcode']

        if User.objects.filter(email=data['email']).exists():
            return Response(data={"error":"EMAIL_EXISTS"},status=status.HTTP_400_BAD_REQUEST)

        serialize = UsersSerializer(data=data)

        if serialize.is_valid():

            try:
                response = sign_up_and_login(data['email'], data['password'])
                user = serialize.save()
                data['user'] = user

                try:
                    profile = Profile.objects.create(
                        user=user,
                        firebase_id=response.user_id,
                        phone=data.get('phone',""),
                        zipcode=data.get('zipcode',""))
                except Exception as e:
                    logger.exception(e)
                    user.delete()

                    return Response(status=status.HTTP_400_BAD_REQUEST)

            except Exception as err:
                logger.exception(err)
                err = str(err)

                if "INVALID_EMAIL" in err:
                    return Response(data={"error":"INVALID_EMAIL"},status=status.HTTP_400_BAD_REQUEST)
                elif "EMAIL_EXISTS" in err:
                    return Response(data={"error":"EMAIL_EXISTS"},status=status.HTTP_400_BAD_REQUEST)
                elif "WEAK_PASSWORD" in err:
                    return Response(data={"error":"WEAK_PASSWORD"},status=status.HTTP_400_BAD_REQUEST)
                else:
                    return Response(data={"error":"UNKNOWN_ERROR"},status=status.HTTP_400_BAD_REQUEST)
                return Response(data={"error":"UNKNOWN_ERROR"},status=status.HTTP_400_BAD_REQUEST)

            return Response(data={"token":response.jwt_token},status=status.HTTP_201_CREATED)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class UserInstanceView(APIView):

    @require_authentication
    def get(self, request, firebase_user_id):
        requesting_user = request.user

        try:
            profile = Profile.objects.get(firebase_id=firebase_user_id)
            user = profile.user
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            check_can_read_user(requesting_user, user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            serialize = ProfileReadSerializer(profile)

            return Response(serialize.data)

    @require_authentication
    def put(self, request, firebase_user_id):
        requesting_user = request.user

        try:
            profile = Profile.objects.get(firebase_id=firebase_user_id)
            user = profile.user
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = request.data

        try:
            check_can_update_user(requesting_user, user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                serialize = UserInstanceWriteSerializer(instance=user, data=data, partial=True)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                if serialize.is_valid():
                    save = serialize.save()

                    update_firebase_user_email(firebase_user_id, save.email)

            data["firebase_id"] = profile.firebase_id # We don't want users to change their Firebase ID
            serializedUser = ProfileWriteSerializer(instance=profile, data=data, partial=True)

            if serializedUser.is_valid():
                serializedUser.save()
                return Response(status=status.HTTP_200_OK)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

    @require_authentication
    def delete(self, request, firebase_user_id):
        requesting_user = request.user

        try:
            profile = Profile.objects.get(firebase_id=firebase_user_id)
            user = profile.user
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            check_can_delete_user(requesting_user, user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                user.delete()
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(status=status.HTTP_204_NO_CONTENT)


class ArtworkView(APIView):

    # UNPROTECTED
    def get(self, request):

        artwork = Artwork.objects.all()
        serialize = ArtworkReadSerializer(artwork, many=True)
        return Response(serialize.data)

    @require_authentication
    def post(self, request):
        requesting_user = request.user
        data = request.data

        try:
            check_can_add_artwork(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            serialize = ArtworkSerializer(data=data)

            if serialize.is_valid():
                try:
                    save = serialize.save()
                except Exception as e:
                    logger.exception(e)
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                else:

                    for x in range(0, len(data['pictures'])):
                        data['pictures'][x]['piece'] = save.id
                        serialized_pic = ArtworkPictureWriteSerializer(data=data['pictures'][x])

                        if serialized_pic.is_valid():
                            serialized_pic.save()
                        else:
                            print(serialized_pic.errors)
                            return Response({"error":"no_pics_two"},status=status.HTTP_201_CREATED)

                    return Response({"id":save.id},status=status.HTTP_201_CREATED)

class ArtworkInstanceView(APIView):

    # UNPROTECTED
    def get(self, request, artwork_id):

        try:
            artwork = Artwork.objects.get(id=artwork_id)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        serialize = ArtworkInstanceReadSerializer(artwork)

        return Response(serialize.data)

    @require_authentication
    def put(self, request, artwork_id):
        requesting_user = request.user
        data = request.data

        try:
            check_can_update_artwork(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                artwork = Artwork.objects.get(id=artwork_id)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_404_NOT_FOUND)

            serialize = ArtworkInstanceWriteSerializer(instance=artwork, data=data, partial=True)

            if serialize.is_valid():
                save = serialize.save()
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

            if "pictures" in data:
                # Assumes that PUT payload only contains new pictures
                for x in range(0, len(data['pictures'])):
                            data['pictures'][x]['piece'] = save.id
                            serialized_pic = ArtworkPictureWriteSerializer(data=data['pictures'][x])

                            if serialized_pic.is_valid():
                                serialized_pic.save()
                            else:
                                print(serialized_pic.errors)
                                return Response({"error":"no_pics"},status=status.HTTP_201_CREATED)

            return Response(status=status.HTTP_200_OK)

    @require_authentication
    def delete(self, request, artwork_id):
        requesting_user = request.user

        try:
            artwork = Artwork.objects.get(id=artwork_id)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            try:
                check_can_delete_artwork(requesting_user)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            else:
                artwork.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

class OrderView(APIView):

    @require_authentication
    def get(self, request):
        requesting_user = request.user

        try:
            check_can_read_all_orders(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            orders = Order.objects.all()
            serialize = OrderReadSerializer(orders, many=True)
            return Response(serialize.data)

    @require_authentication
    def post(self, request):
        requesting_user = request.user
        data = request.data

        try:
            Artwork.objects.get(id=data['piece'])
        except Exception as e:
            logger.exception(e)
            return Response(data={"Artwork not found"},status=status.HTTP_400_BAD_REQUEST)

        try:
            check_can_add_order(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:

            if requesting_user.is_superuser == False and data['customer'] != requesting_user.id:
                return Response(data={"Customer information incorrect"},status=status.HTTP_400_BAD_REQUEST)

            serialize = OrderWriteSerializer(data=data)

            if serialize.is_valid():
                serialize.save()
                return Response(status=status.HTTP_201_CREATED)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

class OrderInstanceView(APIView):

    @require_authentication
    def get(self, request, order_id):
        requesting_user = request.user

        try:
            order = Order.objects.get(id=order_id)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        try:
            check_can_read_order(requesting_user, order)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            serialize = OrderInstanceReadSerializer(order)

            return Response(serialize.data)

    @require_authentication
    def put(self, request, order_id):
        requesting_user = request.user

        try:
            order = Order.objects.get(id=order_id)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)

        data = request.data

        try:
            check_can_update_order(requesting_user, order)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        else:
            try:
                serialize = OrderInstanceWriteSerializer(instance=order, data=data, partial=True)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_400_BAD_REQUEST)
            else:
                if serialize.is_valid():
                    save = serialize.save()
                    return Response(status=status.HTTP_200_OK)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)

    @require_authentication
    def delete(self, request, order_id):
        requesting_user = request.user

        try:
            order = Order.objects.get(id=order_id)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            try:
                check_can_delete_order(requesting_user, order)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            else:
                order.delete()
                return Response(status=status.HTTP_204_NO_CONTENT)

#######################################
###########AUTH ENDPOINTS##############
#######################################

class ForgotPasswordView(APIView):

    # UNPROTECTED
    def post(self, request):

        try:
            email:str=request.data['email']

            send_password_reset_email(email)
        except Exception as err:
            logger.exception(err)
            err = str(err)
            if "EMAIL_NOT_FOUND" in err:
                return Response(data={"error":"EMAIL_NOT_FOUND"},status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response(data={"error":"UNSPECIFIED_ERROR"},status=status.HTTP_401_UNAUTHORIZED)

        return Response(status=status.HTTP_201_CREATED)

class LogoutView(APIView):

    @require_authentication
    def post(self, request):
        requesting_user = request.user

        profile = Profile.objects.get(user = requesting_user)

        try:
            check_can_user_logout(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        try:
            logout(requesting_user.profile.firebase_id)
        except Exception as err:
            logger.exception(err)
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_201_CREATED)

class ChangePasswordView(APIView):

    @require_authentication
    def put(self, request):
        requesting_user = request.user

        profile = Profile.objects.get(user = requesting_user)

        try:
            check_can_change_user_password(requesting_user)
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        try:
            new_password:str = request.data['new_password']
            change_password(profile.firebase_id,new_password)
        except Exception as err:
            logger.exception(err)
            err = str(err)
            if "Password must be" in err:
                return Response(data={"error":"WEAK_PASSWORD"},status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(data={"error":"UNSPECIFIED_ERROR"},status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_201_CREATED)

class RefreshTokenView(APIView):

    # UNPROTECTED
    def post(self, request):

        try:
            jwt:str=get_token_from_headers(request)
            decoded_token:DecodedToken=decode_unverified_token(jwt)

            try:
                profile = Profile.objects.get(firebase_id = decoded_token.user_id)
            except Exception as e:
                logger.exception(e)
                return Response(status=status.HTTP_401_UNAUTHORIZED)

            refresh_token_response:RefreshTokenResponse=refresh_token(profile.refresh_token)

            new_jwt:str=refresh_token_response.jwt_token
            new_refresh_token:str=refresh_token_response.refresh_token

            profile.refresh_token = new_refresh_token
            profile.save()
        except Exception as err:
            logger.exception(err)
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return Response(data={"token":new_jwt},status=status.HTTP_201_CREATED)

class LoginView(APIView):

    # UNPROTECTED
    def post(self, request):

        try:
            email:str=request.data['email']
            password:str=request.data['password']
            resp:TokenResponse=login(email,password)

            profile = Profile.objects.get(firebase_id=resp.user_id)
            profile.refresh_token = resp.refresh_token
            profile.save()

        except Exception as err:
            logger.exception(err)
            err = str(err)
            print(err)
            if "INVALID_PASSWORD" in err:
                return Response(data={"error":"INVALID_CREDENTIALS"},status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(data={"error":"UNSPECIFIED_ERROR"},status=status.HTTP_400_BAD_REQUEST)

        return Response(data={"token":resp.jwt_token},status=status.HTTP_201_CREATED)



#######################################
###########TEST ENDPOINTS##############
#######################################

class ErrorView(APIView):

    # UNPROTECTED
    def get(self, request):
        division_by_zero = 1/0

class DBTestView(APIView):

    # UNPROTECTED
    def get(self, request):
        try:
            User.objects.all().first()
        except Exception as e:
            logger.exception(e)
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_200_OK)