from rest_framework import serializers

from .models import *

class UsersSerializer(serializers.ModelSerializer):

    def create(self, data):
        return User.objects.create(**data)

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email')

class UserInstanceReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email')

class ProfileWriteSerializer(serializers.ModelSerializer):

    def create(self, data):
        return Profile.objects.create(**data)

    def update(self, instance, data):
        instance.zipcode = data.get('zipcode', instance.zipcode)
        instance.phone = data.get('phone', instance.phone)

        instance.save()
        return instance
    class Meta:
        model = Profile
        fields = ('id', 'firebase_id', 'phone', 'zipcode')

class UserInstanceWriteSerializer(serializers.ModelSerializer):

    def update(self, instance, data):
        instance.first_name = data.get('first_name', instance.first_name)
        instance.last_name = data.get('last_name', instance.last_name)
        instance.email = data.get('email', instance.email)
        instance.username = instance.email

        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


# User/Profile Read Serializers

class UsersReadSerializer(serializers.ModelSerializer):
    profile = ProfileWriteSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'profile')

class ProfileReadSerializer(serializers.ModelSerializer):
    user = UserInstanceReadSerializer(read_only=True)

    class Meta:
        model = Profile
        fields = ('id', 'user', 'firebase_id', 'phone', 'zipcode', 'created', 'updated')

class UsersReadSerializer(serializers.ModelSerializer):
    profile = ProfileReadSerializer()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'username', 'email', 'profile')

class ArtworkPictureReadSerializer(serializers.ModelSerializer):

    class Meta:
        model = ArtworkPicture
        fields = ('id', 'piece', 'location', 'created')

class ArtworkPictureWriteSerializer(serializers.ModelSerializer):

    def create(self, data):
        return ArtworkPicture.objects.create(**data)

    class Meta:
        model = ArtworkPicture
        fields = ('piece', 'location')


class ArtworkReadSerializer(serializers.ModelSerializer):
    pictures = ArtworkPictureReadSerializer(many=True)

    class Meta:
        model = Artwork
        fields = ('id', 'artist', 'title', 'date_created', 'artwork_type', 'height_cm', 'width_cm', 'depth_cm', 'price', 'medium', 'description','pictures','created','updated')

class ArtworkSerializer(serializers.ModelSerializer):

    def create(self, data):
        return Artwork.objects.create(**data)

    class Meta:
        model = Artwork
        fields = ('id', 'artist', 'title', 'date_created', 'artwork_type', 'height_cm', 'width_cm', 'depth_cm', 'price', 'medium', 'description')


class ArtworkInstanceReadSerializer(serializers.ModelSerializer):
    pictures = ArtworkPictureReadSerializer(many=True)

    class Meta:
        model = Artwork
        fields = ('id', 'artist', 'title', 'date_created', 'artwork_type', 'height_cm', 'width_cm', 'depth_cm', 'price', 'medium', 'description','pictures','created','updated')

class ArtworkInstanceWriteSerializer(serializers.ModelSerializer):

    def update(self, instance, data):

        instance.artist = data.get('artist', instance.artist)
        instance.title = data.get('title', instance.title)
        instance.date_created = data.get('date_created', instance.date_created)
        instance.artwork_type = data.get('artwork_type', instance.artwork_type)
        instance.height_cm = data.get('height_cm', instance.height_cm)
        instance.width_cm = data.get('width_cm', instance.width_cm)
        instance.depth_cm = data.get('depth_cm', instance.depth_cm)
        instance.price = data.get('price', instance.price)
        instance.medium = data.get('medium', instance.medium)
        instance.description = data.get('description', instance.description)

        instance.save()
        return instance

    class Meta:
        model = Artwork
        fields = ('id', 'artist', 'title', 'date_created', 'artwork_type', 'height_cm', 'width_cm', 'depth_cm', 'price', 'medium', 'description')


class OrderReadSerializer(serializers.ModelSerializer):
    customer = UserInstanceReadSerializer(many=False)
    piece = ArtworkInstanceReadSerializer(many=False)
    class Meta:
        model = Order
        fields = ('id', 'customer', 'piece', 'cost', 'is_submitted', 'is_completed', 'created', 'updated')

class OrderWriteSerializer(serializers.ModelSerializer):

    def create(self, data):
        return Order.objects.create(**data)

    class Meta:
        model = Order
        fields = ('customer', 'piece', 'cost', 'is_submitted', 'is_completed')

class OrderInstanceReadSerializer(serializers.ModelSerializer):
    customer = UserInstanceReadSerializer(many=False)
    piece = ArtworkInstanceReadSerializer(many=False)
    class Meta:
        model = Order
        fields = ('id', 'customer', 'piece', 'cost', 'is_submitted', 'is_completed', 'created', 'updated')

class OrderInstanceWriteSerializer(serializers.ModelSerializer):

    def update(self, instance, data):

        instance.customer = data.get('customer', instance.customer)
        instance.piece = data.get('piece', instance.piece)
        instance.cost = data.get('cost', instance.cost)
        instance.is_submitted = data.get('is_submitted', instance.is_submitted)
        instance.is_completed = data.get('is_completed', instance.is_completed)

        instance.save()
        return instance

    class Meta:
        model = Order
        fields = ('id', 'customer', 'piece', 'cost', 'is_submitted', 'is_completed', 'created', 'updated')