from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.conf import settings
from .models import Artwork, ArtworkPicture, Order
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import status
from .helpers import get_token_from_headers
from .authentication.firebase.firebase_auth import verify_token
from .authentication.firebase.models import DecodedToken


##############################
# User Objects
##############################
def check_can_add_user(requesting_user:User) -> None:
    return

def check_can_read_all_users(requesting_user:User) -> None:
    if _requesting_user_is_superuser(requesting_user):
        return
    else:
        raise PermissionDenied()

def check_can_read_user(requesting_user:User, user:User) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_matches_requested_user(requesting_user,user):
        return
    else:
        raise PermissionDenied()

def check_can_update_user(requesting_user:User, user:User) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_matches_requested_user(requesting_user,user):
        return
    else:
        raise PermissionDenied()

def check_can_delete_user(requesting_user:User, user:User) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_matches_requested_user(requesting_user,user):
        return
    else:
        raise PermissionDenied()

def check_can_user_logout(requesting_user:User) -> None:
    return

def check_can_change_user_password(requesting_user:User) -> None:
    return


##############################
# Artwork Objects
##############################
def check_can_add_artwork(requesting_user:User) -> None:
    if _requesting_user_is_superuser(requesting_user):
        return
    else:
        raise PermissionDenied()

def check_can_read_all_artwork(requesting_user:User) -> None:
    return

def check_can_read_artwork(requesting_user:User) -> None:
    return

def check_can_update_artwork(requesting_user:User) -> None:
    if _requesting_user_is_superuser(requesting_user):
        return
    else:
        raise PermissionDenied()

def check_can_delete_artwork(requesting_user:User) -> None:
    if _requesting_user_is_superuser(requesting_user):
        return
    else:
        raise PermissionDenied()


##############################
# Order Objects
##############################
def check_can_add_order(requesting_user:User) -> None:
    return

def check_can_read_all_orders(requesting_user:User) -> None:
    if _requesting_user_is_superuser(requesting_user):
        return
    else:
        raise PermissionDenied()

def check_can_read_order(requesting_user:User, order:Order) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_is_customer(requesting_user,order):
        return
    else:
        raise PermissionDenied()

def check_can_update_order(requesting_user:User, order:Order) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_is_customer(requesting_user,order):
        return
    else:
        raise PermissionDenied()

def check_can_delete_order(requesting_user:User, order:Order) -> None:
    if _requesting_user_is_superuser(requesting_user) or _requesting_user_is_customer(requesting_user,order):
        return
    else:
        raise PermissionDenied()



##############################
# Wrappers
##############################
def require_authentication(view_func):
    def wrapped_view_func(view_instance, request:Request, *args, **kwargs):
        # Before View Function
        try:
            request = ensure_authenticated(request)
        except Exception as e:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
        func_result = view_func(view_instance, request,*args, **kwargs)
        # After View Function
        return func_result
    return wrapped_view_func

def ensure_authenticated(request:Request) -> Request:
        calling_user=None

        # If debug allow setting user via query string
        if settings.DEBUG and request.GET.get('user'):
            try:
                calling_user = User.objects.get(id=request.GET.get('user'))
                request.user = calling_user
                return request
            except Exception as err:
                pass

            try:
                calling_user = User.objects.get(profile__firebase_id=request.GET.get('user'))
                request.user = calling_user
                return request
            except Exception as err:
                pass

        jwt:str = get_token_from_headers(request)
        decoded_token:DecodedToken=verify_token(jwt)
        calling_user = User.objects.get(profile__firebase_id=decoded_token.user_id)
        request.user = calling_user
        return request

##############################
# Helpers
##############################
def _requesting_user_is_superuser(requesting_user:User) -> bool:
    return requesting_user.is_superuser

def _requesting_user_matches_requested_user(requesting_user:User, requested_user:User) -> bool:
    return requesting_user.id == requested_user.id

def _requesting_user_is_customer(requesting_user:User, order:Order) -> bool:
    return requesting_user.id == order.customer.id