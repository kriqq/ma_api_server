from django.db import models
from django.contrib.auth.models import User

# Profile Model
class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    firebase_id = models.CharField(max_length=255)
    phone = models.CharField(max_length=16)
    zipcode = models.CharField(max_length=8)
    refresh_token = models.CharField(max_length=600, blank=True, default="")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user

# Artwork Model
class Artwork(models.Model):
    Painting = 'PAINTING'
    Sculpture = 'SCULPTURE'
    Printmaking = 'PRINTMAKING'
    Drawing = 'DRAWING'
    Other = 'OTHER'
    PIECE_TYPE_CHOICES =[
        (Painting, 'PAINTING'),
        (Sculpture, 'SCULPTURE'),
        (Printmaking, 'PRINTMAKING'),
        (Drawing, 'DRAWING'),
        (Other, 'OTHER')
    ]
    id = models.AutoField(primary_key=True)
    artist = models.CharField(max_length = 255)
    title = models.CharField(max_length = 255)
    date_created = models.DateTimeField()
    artwork_type = models.CharField(choices=PIECE_TYPE_CHOICES, max_length = 255)
    height_cm = models.DecimalField(max_digits=7, decimal_places=3)
    width_cm = models.DecimalField(max_digits=7, decimal_places=3)
    depth_cm = models.DecimalField(max_digits=7, decimal_places=3)
    price = models.DecimalField(max_digits=9, decimal_places=2)
    medium = models.CharField(max_length = 255)
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

# Artwork Picture Model
class ArtworkPicture(models.Model):
    id = models.AutoField(primary_key=True)
    piece = models.ForeignKey(Artwork, related_name='pictures', on_delete=models.CASCADE)
    location = models.URLField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

# Order Model
class Order(models.Model):
    id = models.AutoField(primary_key=True)
    customer= models.ForeignKey(User, on_delete=models.CASCADE)
    piece = models.ForeignKey(Artwork, on_delete=models.CASCADE)
    cost = models.DecimalField(max_digits=9, decimal_places=2)
    is_submitted = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)