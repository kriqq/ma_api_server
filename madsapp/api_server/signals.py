from django.db.models.signals import post_delete
from django.dispatch import receiver
from .models import User

from api_server.authentication.firebase.firebase_auth import delete_user

@receiver(post_delete, sender=User)
def user_post_delete_handler(sender, instance, **kwargs):
    print(f"user_post_delete_handler for instance {instance}")
    delete_user(instance.profile.firebase_id)
    return
