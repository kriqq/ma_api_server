from django.test import Client, TestCase
from django.db import transaction
from django.db.models import Q
from api_server.models import *
from django.contrib.auth.models import User
from typing import Dict, NoReturn
from unittest import skip, skipIf
import uuid, os, requests, json
from requests import Response

class EndpointTests(TestCase):
    fixtures = ['dump.json']

    users_endpoint="/api/users"
    user_instance_endpoint = lambda self, user_id: f"/api/users/{user_id}"
    artwork_endpoint="/api/artwork"
    artwork_instance_endpoint = lambda self, artwork_id: f"/api/artwork/{artwork_id}"
    orders_endpoint="/api/orders"
    orders_instance_endpoint = lambda self, order_id: f"/api/orders/{order_id}"
    db_endpoint="/api/db-connect-test"

    super_user_token:str
    test_user_one_token:str
    test_user_two_token:str
    test_user_three_token:str

    super_user=None
    test_user_one=None
    test_user_two=None
    test_user_three=None

    test_order=None
    test_order_user=None

    test_artwork=None
    test_artwork_user=None

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        client = Client()
        login_url:str="/api/auth/login"

        email="callmecoppers@gmail.com"
        response = client.post(login_url, {"email": "callmecoppers@gmail.com", "password": "Thinkpad9!"})
        cls.super_user_token=response.json()["token"]
        user = User.objects.get(email=email)
        cls.super_user = user
        assert user.is_superuser

        email="gina@test.com"
        response = client.post(login_url, {"email": "gina@test.com", "password": "password123"})
        cls.test_user_one_token= response.json()["token"]
        user = User.objects.get(email=email)
        cls.test_user_one = user
        assert not user.is_superuser


        email="testera@test.com"
        response = client.post(login_url, {"email": "testera@test.com", "password": "password123"})
        cls.test_user_two_token= response.json()["token"]
        user = User.objects.get(email=email)
        cls.test_user_two = user
        assert not user.is_superuser


        email="testerb@test.com"
        response = client.post(login_url, {"email": "testerb@test.com", "password": "password123"})
        cls.test_user_three_token= response.json()["token"]
        user = User.objects.get(email=email)
        cls.test_user_three = user
        assert not user.is_superuser

    def setUp(self):
        # Runs before every test case
        pass

    ###############################################
    # Users GET
    ###############################################

    def test_users_get_must_be_authenticated(self):
        send_request("get", self.users_endpoint, should_succeed=False)
        send_request("get", self.users_endpoint, auth_token=self.super_user_token, should_succeed=True)

        return

    def test_users_get_only_superuser(self):
        send_request("get", self.users_endpoint, auth_token=self.super_user_token, should_succeed=True)
        send_request("get", self.users_endpoint, auth_token=self.test_user_three_token, should_succeed=False)

        return

    ###############################################
    # Users POST
    ###############################################

    def test_users_post(self):

        # Invalid User
        new_user={
            "first_name":"TestNewUser",
            "last_name":"TestLast",
            "email":"testNewUser@example.com",
        }
        try:
            with transaction.atomic():
                user = send_request("post", self.users_endpoint, data_dict=new_user, should_succeed=False)
        except Exception as e:
            pass

        # Valid User
        new_user={
            "first_name":"TestNewUser",
            "last_name":"TestLast",
            "email":"testNewUser3@example.com",
            "zipcode":"84060",
            "phone":"3013952530",
            "password":"password123",
        }
        user = send_request("post", self.users_endpoint, data_dict=new_user, should_succeed=True)
        user_object = User.objects.get(email=new_user['email'])
        profile_object = Profile.objects.get(user=user_object)
        assert user_object.first_name == new_user["first_name"], f"Expected {new_user['first_name']} but got {user_object.first_name }"
        assert user_object.last_name == new_user["last_name"], f"Expected {new_user['last_name']} but got {user_object.last_name}"
        assert user_object.email == new_user["email"], f"Expected {new_user['email']} but got {user_object.email}"
        assert profile_object.zipcode == new_user["zipcode"], f"Expected {new_user['zipcode']} but got {user_object.zipcode}"
        assert profile_object.phone == new_user["phone"], f"Expected {new_user['phone']} but got {user_object.phone}"
        send_request("delete", self.user_instance_endpoint(user_object.profile.firebase_id), should_succeed=True, auth_token=self.super_user_token)

        return

    ###############################################
    # Users GET Instance
    ###############################################

    def test_user_instance_get_endpoint_must_be_authenticated(self):
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), should_succeed=False)
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), auth_token=self.test_user_one_token, should_succeed=True)

        return

    def test_users_get_only_self_and_superusers(self):
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), auth_token=self.test_user_one_token, should_succeed=True)
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), auth_token=self.test_user_two_token, should_succeed=False)
        send_request("get", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), auth_token=self.test_user_two_token, should_succeed=True)
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), auth_token=self.super_user_token, should_succeed=True)

        return

    def test_user_instance_get_endpoint_superuser_can_get_any(self):
        send_request("get", self.user_instance_endpoint(self.super_user.profile.firebase_id), should_succeed=True, auth_token=self.super_user_token)
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), should_succeed=True, auth_token=self.super_user_token)
        send_request("get", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), should_succeed=True, auth_token=self.super_user_token)
        send_request("get", self.user_instance_endpoint(self.test_user_three.profile.firebase_id), should_succeed=True, auth_token=self.super_user_token)

        return

    def test_user_instance_get_endpoint_user_cannot_get_other_users(self):
        send_request("get", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), should_succeed=False, auth_token=self.test_user_one_token)
        send_request("get", self.user_instance_endpoint(self.test_user_three.profile.firebase_id), should_succeed=False, auth_token=self.test_user_two_token)
        send_request("get", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), should_succeed=False, auth_token=self.test_user_three_token)

        return


    ###############################################
    # User instance PUT
    ###############################################
    def test_user_instance_put_must_be_authenticated(self):
        send_request("put", self.user_instance_endpoint(self.super_user.profile.firebase_id), should_succeed=False)
        return

    def test_user_instance_put_super_user_can_update_any_user(self):

        # Test user
        new_user={
            "first_name":"TestNewUser",
            "last_name":"TestLast",
            "phone":"8005557777",
            "zipcode":"84098",
        }

        send_request("put", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), data_dict=new_user, should_succeed=True, auth_token=self.super_user_token)
        user_object = user_object = User.objects.get(id=self.test_user_one.id)
        profile_object = Profile.objects.get(user=user_object)
        assert user_object.first_name == new_user["first_name"], f"Expected {new_user['first_name']} but got {user_object.first_name }"
        assert user_object.last_name == new_user["last_name"], f"Expected {new_user['last_name']} but got {user_object.last_name}"
        #assert user_object.email == new_user["email"], f"Expected {new_user['email']} but got {user_object.email}"
        send_request("put", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), data_dict={"email":"tester11@test.com"}, should_succeed=True, auth_token=self.super_user_token)
        assert profile_object.zipcode == new_user["zipcode"], f"Expected {new_user['zipcode']} but got {user_object.zipcode}"
        assert profile_object.phone == new_user["phone"], f"Expected {new_user['phone']} but got {user_object.phone}"
        send_request("put", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), data_dict={"email":"gina@test.com"}, should_succeed=True, auth_token=self.super_user_token)


    def test_user_instance_put_others_can_update_self_only(self):
        # Test user
        new_user={
            "first_name":"TestNewUser1",
            "last_name":"TestLast1",
            "phone":"800-555-7777",
            "zipcode":"84098",
        }

        send_request("put", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), data_dict=new_user, should_succeed=True, auth_token=self.test_user_two_token)
        user_object = user_object = User.objects.get(id=self.test_user_two.id)
        profile_object = Profile.objects.get(user=user_object)
        assert user_object.first_name == new_user["first_name"], f"Expected {new_user['first_name']} but got {user_object.first_name}"
        assert user_object.last_name == new_user["last_name"], f"Expected {new_user['last_name']} but got {user_object.last_name}"
        #assert user_object.email == new_user["email"], f"Expected {new_user['email']} but got {user_object.email}"
        send_request("put", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), data_dict={"email":"tester222@test.com"}, should_succeed=True, auth_token=self.super_user_token)
        assert profile_object.zipcode == new_user["zipcode"], f"Expected {new_user['zipcode']} but got {user_object.zipcode}"
        assert profile_object.phone == new_user["phone"], f"Expected {new_user['phone']} but got {user_object.phone}"
        send_request("put", self.user_instance_endpoint(self.test_user_two.profile.firebase_id), data_dict={"email":"testera@test.com"}, should_succeed=True, auth_token=self.super_user_token)

        send_request("put", self.user_instance_endpoint(self.test_user_one.profile.firebase_id), data_dict=new_user, should_succeed=False, auth_token=self.test_user_two_token)
        send_request("put", self.user_instance_endpoint(self.test_user_three.profile.firebase_id), data_dict=new_user, should_succeed=False, auth_token=self.test_user_two_token)
        send_request("put", self.user_instance_endpoint(self.super_user.profile.firebase_id), data_dict=new_user, should_succeed=False, auth_token=self.test_user_two_token)


    ###############################################
    # User instance DELETE
    ###############################################
    def test_user_instance_delete_must_be_authenticated(self):
        new_user={
            "first_name":"TestNewUser",
            "last_name":"TestLast",
            "email":"testNewUser123456@example.com",
            "phone":"800-555-7777",
            "zipcode":"84098",
            "password": "password123",
        }

        response = send_request("post", self.users_endpoint, data_dict=new_user, should_succeed=True)
        user_object = user_object = User.objects.get(email=new_user['email'])
        profile_object = Profile.objects.get(user=user_object)

        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=False)
        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=True, auth_token=self.super_user_token)
        return

    def test_user_instance_delete_super_user_can_delete_any_user(self):
        # Test user
        new_user={
            "first_name":"TestNewUser",
            "last_name":"TestLast",
            "email":"delete_me123@example.com",
            "phone":"800-555-7777",
            "zipcode":"84060",
            "password": "password123",
        }

        response = send_request("post", self.users_endpoint, data_dict=new_user, should_succeed=True)
        user_object=User.objects.get(email=new_user['email'])
        profile_object=Profile.objects.get(user=user_object)

        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=True, auth_token=self.super_user_token)
        return

    def test_user_instance_delete_others_can_delete_self_only(self):
        # Test user
        new_user={
            "first_name":"TestNewUser1",
            "last_name":"TestLast1",
            "email":"delete_me456@example.com",
            "phone":"800-555-7777",
            "zipcode":"84060",
            "password": "password123",
        }

        response = send_request("post", self.users_endpoint, data_dict=new_user, should_succeed=True)
        user_object=User.objects.get(email=new_user['email'])
        profile_object=Profile.objects.get(user=user_object)
        proper_token = response["token"]

        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=False, auth_token=self.test_user_one_token)
        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=False, auth_token=self.test_user_two_token)
        send_request("delete", self.user_instance_endpoint(profile_object.firebase_id), should_succeed=True, auth_token=proper_token)

    ###############################################
    # Artwork GET
    ###############################################

    def test_artwork_get_can_be_unauthenticated(self):
        send_request("get", self.artwork_endpoint, should_succeed=True)
        send_request("get", self.artwork_endpoint, should_succeed=True, auth_token=self.test_user_three_token)
        send_request("get", self.artwork_endpoint, should_succeed=True, auth_token=self.super_user_token)

    ###############################################
    # Artwork POST
    ###############################################

    def test_artwork_post_endpoint_must_be_authenticated(self):
        new_artwork = {
            "artist": "Madeline Copley",
            "title": "testtesttest",
            "date_created": "2020-12-03T17:13:58.559250Z",
            "artwork_type": "SCULPTURE",
            "height_cm": "15.000",
            "width_cm": "20.000",
            "depth_cm": "10.000",
            "price": "160.00",
            "medium": "Clay sculpture and carved wood",
            "description": "Birds on Wood",
            "pictures": [
                {
                    "location": "https://pbs.twimg.com/profile_images/1394627961765175296/eSfXMAsp_400x400.jpg"
                },
                {
                    "location": "https://manofmany.com/wp-content/uploads/2021/06/Hasbulla-Magomedov-4-1200x800.jpg"
                }
            ]
        }

        send_request("post", self.artwork_endpoint, data_dict=new_artwork,  should_succeed=False)
        send_request("post", self.artwork_endpoint, data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        artwork_object = Artwork.objects.get(title="testtesttest")
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_artwork_post_endpoint_must_be_superuser(self):
        new_artwork = {
            "artist": "Madeline Copley",
            "title": "testtesttest",
            "date_created": "2020-12-03T17:13:58.559250Z",
            "artwork_type": "SCULPTURE",
            "height_cm": "15.000",
            "width_cm": "20.000",
            "depth_cm": "10.000",
            "price": "160.00",
            "medium": "Clay sculpture and carved wood",
            "description": "Birds on Wood",
            "pictures": [
                {
                    "location": "https://pbs.twimg.com/profile_images/1394627961765175296/eSfXMAsp_400x400.jpg"
                },
                {
                    "location": "https://manofmany.com/wp-content/uploads/2021/06/Hasbulla-Magomedov-4-1200x800.jpg"
                }
            ]
        }

        send_request("post", self.artwork_endpoint, data_dict=new_artwork,  should_succeed=False, auth_token=self.test_user_one_token)
        send_request("post", self.artwork_endpoint, data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        artwork_object = Artwork.objects.get(title="testtesttest")
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_artwork_post_endpoint_pictures_arent_necessary(self):
        new_artwork = {
            "artist": "Madeline Copley",
            "title": "testtesttest",
            "date_created": "2020-12-03T17:13:58.559250Z",
            "artwork_type": "SCULPTURE",
            "height_cm": "15.000",
            "width_cm": "20.000",
            "depth_cm": "10.000",
            "price": "160.00",
            "medium": "Clay sculpture and carved wood",
            "description": "Birds on Wood",
            "pictures": [
            ]
        }


        send_request("post", self.artwork_endpoint, data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        artwork_object = Artwork.objects.get(title="testtesttest")
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=True, auth_token=self.super_user_token)


    ###############################################
    # Artwork GET Instance
    ###############################################

    def test_artwork_get_instance_can_be_unauthenticated(self):
        send_request("get", self.artwork_instance_endpoint(1), should_succeed=True)
        send_request("get", self.artwork_instance_endpoint(1), should_succeed=True, auth_token=self.test_user_three_token)
        send_request("get", self.artwork_instance_endpoint(1), should_succeed=True, auth_token=self.super_user_token)


    ###############################################
    # Artwork PUT
    ###############################################

    def test_artwork_put_endpoint_can_update_partial(self):
        new_artwork = {
            "title": "Bird Sculpture 2.0",
        }

        revert = {
            "title": "Bird Sculpture",
        }

        send_request("put", self.artwork_instance_endpoint(10), data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        send_request("put", self.artwork_instance_endpoint(10), data_dict=revert, should_succeed=True, auth_token=self.super_user_token)

    def test_artwork_put_endpoint_only_superuser_can_update(self):
        new_artwork = {
            "title": "Bird Sculpture 2.0",
        }

        revert = {
            "title": "Bird Sculpture",
        }

        send_request("put", self.artwork_instance_endpoint(10), data_dict=new_artwork, should_succeed=False, auth_token=self.test_user_two_token)
        send_request("put", self.artwork_instance_endpoint(10), data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        send_request("put", self.artwork_instance_endpoint(10), data_dict=revert, should_succeed=True, auth_token=self.super_user_token)

    ###############################################
    # Artwork DELETE
    ###############################################

    def test_artwork_delete_endpoint_must_be_authenticated(self):
        new_artwork = {
                "artist": "Madeline Copley",
                "title": "testtesttest",
                "date_created": "2020-12-03T17:13:58.559250Z",
                "artwork_type": "SCULPTURE",
                "height_cm": "15.000",
                "width_cm": "20.000",
                "depth_cm": "10.000",
                "price": "160.00",
                "medium": "Clay sculpture and carved wood",
                "description": "Birds on Wood",
                "pictures": [
                    {
                        "location": "https://pbs.twimg.com/profile_images/1394627961765175296/eSfXMAsp_400x400.jpg"
                    },
                    {
                        "location": "https://manofmany.com/wp-content/uploads/2021/06/Hasbulla-Magomedov-4-1200x800.jpg"
                    }
                ]
            }

        send_request("post", self.artwork_endpoint, data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        artwork_object = Artwork.objects.get(title="testtesttest")
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=False)
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_artwork_delete_endpoint_only_superuser_can_dlt(self):
        new_artwork = {
                "artist": "Madeline Copley",
                "title": "testtesttest",
                "date_created": "2020-12-03T17:13:58.559250Z",
                "artwork_type": "SCULPTURE",
                "height_cm": "15.000",
                "width_cm": "20.000",
                "depth_cm": "10.000",
                "price": "160.00",
                "medium": "Clay sculpture and carved wood",
                "description": "Birds on Wood",
                "pictures": [
                    {
                        "location": "https://pbs.twimg.com/profile_images/1394627961765175296/eSfXMAsp_400x400.jpg"
                    },
                    {
                        "location": "https://manofmany.com/wp-content/uploads/2021/06/Hasbulla-Magomedov-4-1200x800.jpg"
                    }
                ]
            }

        send_request("post", self.artwork_endpoint, data_dict=new_artwork, should_succeed=True, auth_token=self.super_user_token)
        artwork_object = Artwork.objects.get(title="testtesttest")
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=False, auth_token=self.test_user_three_token)
        send_request("delete", self.artwork_instance_endpoint(artwork_object.id), should_succeed=True, auth_token=self.super_user_token)


    ###############################################
    # Orders GET
    ###############################################

    def test_orders_endpoint_must_be_authenticated(self):
        send_request("get", self.orders_endpoint, should_succeed=False)
        send_request("get", self.orders_endpoint, should_succeed=True, auth_token=self.super_user_token)

    def test_orders_endpoint_only_superusers_can_get_all(self):
        send_request("get", self.orders_endpoint, should_succeed=False, auth_token=self.test_user_one_token)
        send_request("get", self.orders_endpoint, should_succeed=False, auth_token=self.test_user_two_token)
        send_request("get", self.orders_endpoint, should_succeed=False, auth_token=self.test_user_three_token)
        send_request("get", self.orders_endpoint, should_succeed=True, auth_token=self.super_user_token)

    ###############################################
    # Orders POST
    ###############################################

    def test_orders_post_must_be_authenticated(self):

        new_order={
        "customer": 22,
        "piece": 16,
        "cost": 999.00,
        "is_submitted": True,
        "is_completed": False
        }


        send_request("post", self.orders_endpoint, data_dict=new_order,  should_succeed=False)
        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)
        order_object = Order.objects.get(cost=999)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_post_any_user_can_post(self):

        new_order={
            "customer": 26,
            "piece": 16,
            "cost": 999.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.test_user_two_token)
        order_object = Order.objects.get(cost=999)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_post_user_cannot_post_for_other_user(self):

        new_order={
            "customer": 26,
            "piece": 16,
            "cost": 999.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=False, auth_token=self.test_user_one_token)
        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=False, auth_token=self.test_user_three_token)
        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.test_user_two_token)
        order_object = Order.objects.get(cost=999)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_post_superuser_can_post_for_any(self):

        new_order={
            "customer": 27,
            "piece": 16,
            "cost": 999.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)
        order_object = Order.objects.get(cost=999)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.super_user_token)


    ###############################################
    # Orders GET Instance
    ###############################################

    def test_orders_get_instance_must_be_authenticated(self):

        send_request("get", self.orders_instance_endpoint(2), should_succeed=False)
        send_request("get", self.orders_instance_endpoint(2), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_get_instance_superuser_can_get_any(self):

        send_request("get", self.orders_instance_endpoint(2), should_succeed=True, auth_token=self.super_user_token)
        send_request("get", self.orders_instance_endpoint(4), should_succeed=True, auth_token=self.super_user_token)
        send_request("get", self.orders_instance_endpoint(12), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_get_instance_user_can_only_get_self(self):

        send_request("get", self.orders_instance_endpoint(5),should_succeed=False, auth_token=self.test_user_three_token)
        send_request("get", self.orders_instance_endpoint(10),should_succeed=True, auth_token=self.test_user_three_token)


    ###############################################
    # Orders PUT
    ###############################################

    def test_orders_put_must_be_authenticated(self):

        new_order={
            "cost":250.00
        }

        send_request("put", self.orders_instance_endpoint(5), data_dict=new_order, should_succeed=False)
        send_request("put", self.orders_instance_endpoint(5), data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)

    def test_orders_superuser_can_update_any(self):

        new_order={
            "cost":500.00
        }

        send_request("put", self.orders_instance_endpoint(7), data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)
        send_request("put", self.orders_instance_endpoint(8), data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)
        send_request("put", self.orders_instance_endpoint(9), data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)


    def test_orders_user_can_only_update_own_order(self):

        new_order={
            "cost":750.00
        }

        send_request("put", self.orders_instance_endpoint(11), data_dict=new_order, should_succeed=False, auth_token=self.test_user_one_token)
        send_request("put", self.orders_instance_endpoint(11), data_dict=new_order, should_succeed=True, auth_token=self.test_user_three_token)


    ###############################################
    # Orders DELETE
    ###############################################

    def test_orders_delete_must_be_authenticated(self):

        new_order={
            "customer": 27,
            "piece": 16,
            "cost": 200.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=False)
        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)

    def test_orders_delete_superuser_can_delete_any(self):

        new_order={
            "customer": self.test_user_one.id,
            "piece": 16,
            "cost": 978.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.super_user_token)
        order_object = Order.objects.get(cost=new_order['cost'])
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.super_user_token)

    def test_orders_delete_user_can_only_delete_own(self):

        new_order={
            "customer": self.test_user_one.id,
            "piece": 16,
            "cost": 978.00,
            "is_submitted": True,
            "is_completed": False
        }

        send_request("post", self.orders_endpoint, data_dict=new_order, should_succeed=True, auth_token=self.test_user_one_token)
        order_object = Order.objects.get(cost=new_order['cost'])
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=False, auth_token=self.test_user_two_token)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=False, auth_token=self.test_user_three_token)
        send_request("delete", self.orders_instance_endpoint(order_object.id), should_succeed=True, auth_token=self.test_user_one_token)


    ###############################################
    # Database Test Endpoint
    ###############################################

    def test_db_test_endpoint_functionality(self):

        send_request("get", self.db_endpoint, should_succeed=True)


####################################
########### HELPERS ################
####################################

def send_request(request_type:str ,url:str, should_succeed:bool=True, data_dict:Dict={}, auth_token:str="", print_response:bool=False):
    client = Client()
    response=None
    headers={
        "HTTP_AUTHORIZATION":f"token {auth_token}",
    }
    if request_type.lower()=="get":
        response = client.get(url, content_type="application/json", **headers)
    elif request_type.lower()=="post":
        response = client.post(url, data_dict, content_type="application/json", **headers)
    elif request_type.lower()=="put":
        response = client.put(url, data_dict, content_type="application/json", **headers)
    elif request_type.lower()=="delete":
        response = client.delete(url, content_type="application/json", **headers)

    if print_response:
        print(f"{url}: {response.status_code}")

    if should_succeed:
        assert (response.status_code<300 and response.status_code>=200) , f"Expected successful status code, but got {response.status_code}"
    else:
        assert (response.status_code>=300 or response.status_code<200) , f"Expected failing status code, but got {response.status_code}"

    return response.json() if getattr(response,"data",False) else None
