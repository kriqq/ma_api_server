# Generated by Django 3.2.9 on 2021-12-03 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_server', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artwork',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='artworkpicture',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='order',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='profile',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
