from django.urls import path

from . import views
from .views import *

class URLRoutes():
    users_base:str ="users"
    artwork_base:str ="artwork"
    orders_base:str ="orders"
    auth_base:str ="auth"
    database_base:str ="db-connect-test"

    api_root =""

    users:str =f"{users_base}"
    user_detail:str =f"{users_base}/<str:firebase_user_id>"

    artwork:str =f"{artwork_base}"
    artwork_detail:str =f"{artwork_base}/<str:artwork_id>"

    orders:str =f"{orders_base}"
    orders_detail:str =f"{orders_base}/<str:order_id>"

    login = f"{auth_base}/login"
    logout = f"{auth_base}/logout"
    forgot_password = f"{auth_base}/forgot-password"
    change_password = f"{auth_base}/change-password"
    refresh_token = f"{auth_base}/refresh-token"


urlpatterns = [
    path(URLRoutes.users, UserView.as_view()),
    path(URLRoutes.user_detail, UserInstanceView.as_view()),
    path(URLRoutes.artwork, ArtworkView.as_view()),
    path(URLRoutes.artwork_detail, ArtworkInstanceView.as_view()),
    path(URLRoutes.orders, OrderView.as_view()),
    path(URLRoutes.orders_detail, OrderInstanceView.as_view()),
    path(URLRoutes.login, LoginView.as_view()),
    path(URLRoutes.logout, LogoutView.as_view()),
    path(URLRoutes.forgot_password, ForgotPasswordView.as_view()),
    path(URLRoutes.change_password, ChangePasswordView.as_view()),
    path(URLRoutes.refresh_token, RefreshTokenView.as_view()),
    path('sentry-debug/', ErrorView.as_view()),
    path(URLRoutes.database_base, DBTestView.as_view())
]
