from django.apps import AppConfig


class ApiServerConfig(AppConfig):
    name = 'api_server'

    def ready(self):
        import api_server.signals