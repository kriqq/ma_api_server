from os import environ
import pyrebase
import firebase_admin
from firebase_admin import credentials, auth
from firebase_admin._token_gen import _JWTVerifier
from typing import Dict, Any, Optional

from .models import TokenResponse, DecodedToken, RefreshTokenResponse

# Config variables should be set via env vars in the settings file
try:
    cred = credentials.Certificate(environ["PATH_TO_FIREBASE_SERVICE_ACCOUNT_KEY"])
    firebase_admin.initialize_app(cred)

    config = {
        "apiKey": environ["API_KEY"],
        "authDomain": environ["AUTH_DOMAIN"],
        "databaseURL": environ["DATABASE_URL"],
        "storageBucket": environ["STORAGE_BUCKET"],
    }
except KeyError as err:
    raise KeyError(f"Must include proper environment variables for firebase: missing {err}")

firebase = pyrebase.initialize_app(config)
# Pyrebase: https://github.com/thisbejim/Pyrebase#authentication
pyrebase_auth = firebase.auth()


# Sign Up
# https://firebase.google.com/docs/reference/rest/auth#section-create-email-password
def sign_up_and_login(email:str,password:str)->TokenResponse:
    # Throws error if email already exists or password < 6 chars
    resp:TokenResponse = TokenResponse(pyrebase_auth.create_user_with_email_and_password(email,password))
    send_email_verification(resp.jwt_token)
    return resp

def send_email_verification(token:str)->Dict[str,Any]:
    return pyrebase_auth.send_email_verification(token)

# Login
# https://github.com/thisbejim/Pyrebase#authentication
# https://firebase.google.com/docs/reference/rest/auth#section-sign-in-email-password
def login(email:str,password:str)->TokenResponse:
    # Throws error if invalid password or email
    resp:TokenResponse = TokenResponse(pyrebase_auth.sign_in_with_email_and_password(email,password))
    return resp

# Logout
# https://firebase.google.com/docs/reference/admin/python/firebase_admin.auth#revoke_refresh_tokens
def logout(user_id:str)->None:
    return revoke_refresh_tokens(user_id)


# Change password
# https://firebase.google.com/docs/auth/admin/manage-users#update_a_user
# https://firebase.google.com/docs/reference/rest/auth#section-change-password

def change_password(user_id:str,new_password:str):
    # Throws error if password < 6 chars
    user = auth.update_user(
        user_id,
        password=new_password,
        )
    return user

def update_firebase_user_email(user_id:str,new_email:str):
    user = auth.update_user(
        user_id,
        email=new_email,
        )
    return user


def update_firebase_user_display_name(user_id:str,new_display_name:str):
    user = auth.update_user(
        user_id,
        display_name=new_display_name,
        )
    return user


def update_firebase_user_phone(user_id:str,new_phone:str):
    user = auth.update_user(
        user_id,
        phone_number=new_phone,
        )
    return user

def update_firebase_user_disabled(user_id:str,disabled_status:bool):
    user = auth.update_user(
        user_id,
        disabled=disabled_status,
        )
    return user

# Forgot Password - Send Reset Email
# https://github.com/thisbejim/Pyrebase#authentication
def send_password_reset_email(email:str)->Dict[str,Any]:
    # Throws error if email doesnt exist
    return pyrebase_auth.send_password_reset_email(email)

# Verify Reset Password Code
# https://firebase.google.com/docs/reference/rest/auth#section-verify-password-reset-code

# Reset Password
# https://firebase.google.com/docs/reference/rest/auth#section-confirm-reset-password


# Delete user
# https://firebase.google.com/docs/auth/admin/manage-users#delete_a_user
def delete_user(user_id:str)->None:
    auth.delete_user(user_id)
    return

# https://firebase.google.com/docs/reference/admin/python/firebase_admin.auth#verify_id_token
def verify_token(token:str)->DecodedToken:
    # Throws error if token expired or malformed
    decoded_token_dict = auth.verify_id_token(token)
    return DecodedToken(decoded_token_dict)

def decode_unverified_token(token:str)->DecodedToken:
    # Throws error if token expired or malformed
    jwt_verifier = auth._get_auth_service(None).token_verifier.id_token_verifier
    decoded_token_tuple=jwt_verifier._decode_unverified(token)
    # decoded_token_dict = auth.verify_id_token(token)
    # print(decoded)
    # return
    return DecodedToken(decoded_token_tuple[1])

# https://firebase.google.com/docs/reference/admin/python/firebase_admin.auth#revoke_refresh_tokens
def revoke_refresh_tokens(user_id:str)->None:
    # Throws error if incorrect user id
    auth.revoke_refresh_tokens(user_id)
    return

# https://github.com/thisbejim/Pyrebase#authentication
def refresh_token(refresh_token:str)->RefreshTokenResponse:
    # Throws error if invalid or revoked refresh token
    resp:RefreshTokenResponse = RefreshTokenResponse(pyrebase_auth.refresh(refresh_token))
    return resp




if __name__ == "__main__":
    pass

