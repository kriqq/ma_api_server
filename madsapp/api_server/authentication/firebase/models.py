from typing import Optional, Dict, Any

class TokenResponse:
    def __init__(self,response:Dict):
        self.user_id:str = response["localId"] 
        self.email:str = response["email"] 
        self.jwt_token:str = response["idToken"] 
        self.refresh_token:str = response["refreshToken"]
        return

class RefreshTokenResponse:
    def __init__(self,response:Dict):
        self.user_id:str = response["userId"] 
        self.jwt_token:str = response["idToken"] 
        self.refresh_token:str = response["refreshToken"]
        return

class DecodedToken:
    def __init__(self,decoded_token:Dict):
        self.user_id:str = decoded_token["user_id"]
        self.exp:int = decoded_token["exp"] 
        self.email:str = decoded_token["email"]
        self.email_is_verified:bool = decoded_token["email_verified"] 
        return

