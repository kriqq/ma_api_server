from typing import Any, Dict
import jwt
from datetime import datetime, timedelta
from django.conf import settings

def create_access_token(firebase_user_id:str,client_id:str,refresh_token:str)->str:
    td:timedelta = timedelta(minutes=10)
    expiration_timestamp:int = generate_expiration_timestamp_in_seconds(td)
    encoded = jwt.encode({"firebase_user_id": firebase_user_id , "client_id":client_id, "refresh_token":refresh_token, "exp":expiration_timestamp}, settings.SECRET_KEY , algorithm='HS256')
    return encoded.decode("utf-8")

def generate_expiration_timestamp_in_seconds(time_delta:timedelta)->int:
    now:datetime = datetime.now()
    future_datetime:datetime = now + time_delta
    return int(future_datetime.timestamp())

def seconds_until_expiration(future_timestamp_in_seconds:int):
    now:datetime = datetime.now()
    current_timestamp = int(now.timestamp())
    return future_timestamp_in_seconds - current_timestamp

def decode_token(token:str)->Dict[str,Any]:
    return jwt.decode(token, settings.SECRET_KEY , algorithm='HS256')

