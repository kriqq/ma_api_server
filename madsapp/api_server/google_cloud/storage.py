from django.conf import settings

# DOCS: https://github.com/googleapis/python-storage
from google.cloud import storage

import datetime
from urllib.parse import unquote

def upload_file(file_obj,blob_uri,make_public=False,download_by_default=True):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(settings.CLOUD_STORAGE_BUCKET_NAME)
    blob = bucket.blob(blob_uri)
    file_obj.seek(0)
    blob.upload_from_file(file_obj,content_type=file_obj.content_type)
    if make_public:
        blob.make_public()
        blob_uri = blob.public_url
    return blob_uri

def delete_file(url):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(settings.CLOUD_STORAGE_BUCKET_NAME)
    blob = bucket.blob( _url_to_blob_uri(url) )
    blob.delete()
    return


def generate_signed_url(blob_uri):
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(settings.CLOUD_STORAGE_BUCKET_NAME)
    blob = bucket.blob(blob_uri)
    url = blob.generate_signed_url(
        version='v4',
        # This URL is valid for 1 hour
        expiration=datetime.timedelta(minutes=60),
        # Allow GET requests using this URL.
        method='GET')
    return url

def _url_to_blob_uri(url):
    uri = unquote( url.split( f"{settings.CLOUD_STORAGE_BUCKET_NAME}/" )[-1] )
    return uri

