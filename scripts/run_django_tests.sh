#! /bin/bash

export DJANGO_SETTINGS_MODULE=madsapp.settings
pipenv run python ./madsapp/manage.py test -r --noinput --verbosity 2 api_server
