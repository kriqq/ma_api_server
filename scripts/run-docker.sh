#! /bin/bash
DOCKER_IMAGE="gcr.io/mads-art-server-334904/django-api-server"
DJANGO_SETTINGS_MODULE="madsapp.settings"
API_CONTAINER_NAME="django-api-server"
DB_CONTAINER_NAME="postgres"
API_CONTAINER_PORT="8080"
DB_CONTAINER_PORT="5431"
DB_CONTAINER_HOST="host.docker.internal"
DB_NAME="ma-art"
DB_USER="postgres"
DB_PASSWORD="password"
API_FIXTURE="dump.json"

# If docker image not built, build it 
if [[ "$(docker images -q $DOCKER_IMAGE 2> /dev/null)" == "" ]]; then
    # do something
    DIR="$(cd "$(dirname "$0")" && pwd)"
    $DIR/build-docker.sh
fi

# Kill and rm current containers
docker kill $API_CONTAINER_NAME
docker rm $API_CONTAINER_NAME
docker kill $DB_CONTAINER_NAME
docker rm $DB_CONTAINER_NAME

# Run postgres and API server in background
docker run --name $DB_CONTAINER_NAME -p $DB_CONTAINER_PORT:5432 -e POSTGRES_USER=$DB_USER -e POSTGRES_PASSWORD=$DB_PASSWORD -e POSTGRES_DB=$DB_NAME -d postgres
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    docker run --network host --name $API_CONTAINER_NAME -p $API_CONTAINER_PORT:$API_CONTAINER_PORT --env PORT=$API_CONTAINER_PORT --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE --env DB_PORT=$DB_CONTAINER_PORT --env DB_HOST=0.0.0.0 -d $DOCKER_IMAGE
else
    docker run --name $API_CONTAINER_NAME -p $API_CONTAINER_PORT:$API_CONTAINER_PORT --env PORT=$API_CONTAINER_PORT --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE --env DB_PORT=$DB_CONTAINER_PORT --env DB_HOST=$DB_CONTAINER_HOST -d $DOCKER_IMAGE
fi

# Wait for containers to initialize
echo "Waiting for containers to start..."
until [ "`/usr/bin/docker inspect -f {{.State.Running}} $API_CONTAINER_NAME`"=="true" ] && [ "`/usr/bin/docker inspect -f {{.State.Running}} $DB_CONTAINER_NAME`"=="true" ]; do
    sleep 0.1;
done;
sleep 2;

# Run migration and load fixture
echo "Running migrations..."
docker exec  $API_CONTAINER_NAME pipenv run python manage.py migrate
echo "Loading fixtures..."
docker exec  $API_CONTAINER_NAME pipenv run python manage.py loaddata $API_FIXTURE

# Log to console
docker logs -f $API_CONTAINER_NAME
