#! /bin/bash

export DJANGO_SETTINGS_MODULE=madsapp.settings
pipenv run python ./madsapp/manage.py test -r --noinput --verbosity 2 api_server.tests.EndpointTests.test_user_instance_delete_others_can_delete_self_only 
